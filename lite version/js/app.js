var routerApp = angular.module('myAdmin', ['ui.router']);

//ui routing
routerApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'dashboard.html'
            })

            .state('products', {
                url: '/products',
                templateUrl: 'products.html'
            })
            .state('category', {
                url: '/category',
                templateUrl: 'category.html'
            })

            .state('settings', {
                url: '/settings',
                templateUrl: 'settings.html'
            })
            .state('offers', {
            });
    });


 /*routerApp.contoller('MainCtrl',function MainCtrl() {});

       MainCtrl.$inject = ['$scope','MainFactory'];

       function MainCtrl($scope, MainFactory) {
              $scope.details = MainFactory.details;
              function init() {
              MainFactory.get();
              }

              init();

              $scope.detailsModel = {
                    "product_id":1,
                          "product_name":"Samsung ON Pro",
                          "product_cost":12000
                          };

              $scope.add = function () {
                    $scope.details.push($scope.detailsModel);
                    };

              $scope.delete = function (index) {
                    $scope.details.splice(index, 1);
              };

              $scope.edited = -1;
              $scope.editedModel = {
                    "product_id":0,
                    "product_name":"",
                    "product_cost":
                    };

              $scope.edit = function (index) {
                    $scope.edited = index;
                    };

              $scope.finishEdit = function (index) {
                    $scope.details[index] = $scope.editedModel;
                    $scope.edited = -1;
                    };


              };


*/



